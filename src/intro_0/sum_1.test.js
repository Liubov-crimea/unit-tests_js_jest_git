
const {sum, nativeNull} = require('./sum_1');   //импорт const из осн док

describe ('Sum function', ()=>{
    test ('Корректное значение', () => {   
        expect(sum(1, 3)).toBe(4)
       })
    
    test ('Корректное значение', () => {   
        expect(sum(1, 3)).toEqual(4)
       })

    test ('> or <', () => {
        expect(sum(2, 3)).toBeGreaterThan(4)
        expect(sum(2, 3)).toBeGreaterThanOrEqual(5)
        expect(sum(2, 3)).toBeLessThan(10)
        expect(sum(2, 3)).toBeLessThanOrEqual(5)
    })  
    
    test ('float values', () => {
        expect(sum(0.1, 0.2)).toBeCloseTo(0.3)  
    
    })
})

describe ('Native null function', ()=>{
    test('should return false value null', ()=>{
        expect(nativeNull()).toBe(null)
        expect(nativeNull()).toBeNull()
        expect(nativeNull()).toBeFalsy() //undefined, null, ' '
        expect(nativeNull()).not.toBeTruthy() //=false
        expect(nativeNull()).not.toBeUndefined()
        expect(nativeNull()).toBeDefined() //значение определено
    })
})


//npm run test sum_1.test.js