const squareWithMock = require('./square_with_mock')

describe ('squareWithMock', () => {
    
    test ('Корректное значение с 2', () => {   //https://jestjs.io/ru/docs/mock-function-api
     const spyMathPow = jest.spyOn(Math, 'pow');   
     squareWithMock(2); 
     expect(spyMathPow).toBeCalledTimes(1)  

    })

    test ('Корректное значение с 1', () => {   
        const spyMathPow = jest.spyOn(Math, 'pow');   
        squareWithMock(1); 
        expect(spyMathPow).toBeCalledTimes(0)  
   
       })

     //моки накапливаются! очищать после каждого теста!!!
     afterEach(()=>{
        jest.clearAllMocks()
     })  
})


//npm run test square_with_mock.test.js