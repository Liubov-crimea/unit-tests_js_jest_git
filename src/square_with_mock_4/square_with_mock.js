const squareWithMock = (number) => {
    if(number === 1) {
        return 1;
    }
    return Math.pow(number, 2) //Math.pow(число, в какую степень)  
}

module.exports = squareWithMock;