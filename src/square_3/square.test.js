const square = require('./square')

describe ('square', () => {
    
    test ('Корректное значение', () => {
        expect(square(2)).toBe(4);  // = 4
        expect(square(2)).toBeLessThan(5); //меньше 5
        expect(square(2)).toBeGreaterThan(3); //больше 3
        expect(square(2)).not.toBeUndefined();//не Undefined

    })
})


//npm run test square.test.js