const randomInt = require('./random_03')

describe('Тестируем случайное число', () => {
    const testCases = [
        {
            from: 100, //указываем диапазон
            to: 110
        },
        {
            from: 200,
            to: 201
        }
    ];
    testCases.forEach(test => {
        it (
            `from : ${test.from} to: ${test.to}`,
            ()=>{
                const res = randomInt(test.from, test.to);
                console.log(res);
                expect(res).toBeGreaterThanOrEqual(test.from);//>=
                expect(res).toBeLessThanOrEqual(test.to);//<=
            }
        );
    });

});

//npm run test random_03.test.js