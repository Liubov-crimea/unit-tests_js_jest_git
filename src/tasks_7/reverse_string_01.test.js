const reverseString = require('./reverse_string_01')

describe('reverseString', ()=>{

    const testCaseValue = [
        {
            inString: 'hello',
            expected: 'olleh'
        },
        {
            inString: 'bin ha',
            expected: 'ah nib'
        },
        {
            inString: 'привет',
            expected: 'тевирп'
        }
    ];

    testCaseValue.forEach(test => {   //помещаем тест в форич
        it (
    `Входящая строка: ${test.inString} ожидаю: ${test.expected}`,  //1 знач
    () => {                                                        //2 знач
           const res = reverseString(test.inString);
           expect(res).toBe(test.expected);
          }  
        );
    })

    /* без подстановки значений
    test('Проверяем реверс строки', ()=>{                        //1 знач
        expect(reverseString('hello www')).toBe('www olleh');    //2 знач
   }, 1000)*/                                                    //3 ожидание +/-

})
    


//npm run test reverse_string_01.test.js