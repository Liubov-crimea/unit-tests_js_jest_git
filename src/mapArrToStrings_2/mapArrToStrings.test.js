const mapArrToStrings = require('./mapArrToStrings');

describe ('mapArrToStrings', () => {
    test('Корректное значение', () => {
        expect(mapArrToStrings([1, 2, 3])).toEqual(['1', '2', '3']); //сравнить объекты этим методом не туби
    })
    test('Смешанные значения', () => {
        expect(mapArrToStrings([1, 2, 3, null, undefined, 'dkfhjkdh'])).toEqual(['1', '2', '3']);
    })
    test('Пустой массив', () => {
        expect(mapArrToStrings([])).toEqual([]);
    })
    test('Отрицание', () => {
        expect(mapArrToStrings[1, 2, 3, 4]).not.toEqual(['1', '2', '3']);
    })
    test('Больше корректного значения', () => {
        expect(mapArrToStrings[1, 2, 3]).not.toBeNull();
    })
})

//npm run test mapArrToStrings.test.js